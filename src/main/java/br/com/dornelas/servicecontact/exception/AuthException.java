package br.com.dornelas.servicecontact.exception;

import br.com.dornelas.servicecontact.exception.object.CodeError;
import org.springframework.http.HttpStatus;

public class AuthException  extends ErrorHandlingException {
    public AuthException(String message) {
        super(message, CodeError.AUTHENTICATION, HttpStatus.UNAUTHORIZED);
    }
}
