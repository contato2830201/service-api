package br.com.dornelas.servicecontact.exception;

import br.com.dornelas.servicecontact.exception.object.CodeError;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.springframework.http.HttpStatus;

@Getter
public class ConflictingDataException extends ErrorHandlingException {
    public ConflictingDataException(String message) {
        super(message, CodeError.CONFLITING_DATA, HttpStatus.CONFLICT);
    }
}
