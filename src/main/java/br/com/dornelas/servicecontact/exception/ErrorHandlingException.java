package br.com.dornelas.servicecontact.exception;

import br.com.dornelas.servicecontact.exception.object.CodeError;
import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.springframework.http.HttpStatus;

@Getter
public class ErrorHandlingException extends RuntimeException {

    private CodeError code;
    private HttpStatus status;
    public ErrorHandlingException(String message, CodeError code, HttpStatus status) {
        super(message);
        this.code = code;
        this.status = status;
    }
}
