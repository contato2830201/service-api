package br.com.dornelas.servicecontact.exception;

import br.com.dornelas.servicecontact.exception.object.CodeError;
import org.springframework.http.HttpStatus;

public class InvalidDataException extends ErrorHandlingException {
    public InvalidDataException(String message) {
        super(message, CodeError.INVALID_DATA, HttpStatus.UNPROCESSABLE_ENTITY);
    }
}
