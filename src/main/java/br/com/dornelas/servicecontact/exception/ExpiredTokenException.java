package br.com.dornelas.servicecontact.exception;

import br.com.dornelas.servicecontact.exception.object.CodeError;
import org.springframework.http.HttpStatus;

public class ExpiredTokenException extends ErrorHandlingException {
    public ExpiredTokenException(String message) {
        super(message, CodeError.EXPIRED_TOKEN, HttpStatus.UNAUTHORIZED);
    }
}
