package br.com.dornelas.servicecontact.exception;

import br.com.dornelas.servicecontact.exception.object.CodeError;
import br.com.dornelas.servicecontact.exception.object.ErrorObject;
import br.com.dornelas.servicecontact.exception.object.ErrorObjectRequiredFields;
import br.com.dornelas.servicecontact.exception.object.RequiredField;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.nio.file.AccessDeniedException;
import java.time.LocalDateTime;
import java.util.List;

@ControllerAdvice
public class ResourceExceptionHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorObject> handlerMethodArgumentNotValidException(MethodArgumentNotValidException e) {
        var requiredFields = getRequiredFields(e.getBindingResult().getFieldErrors());
        ErrorObject error = ErrorObjectRequiredFields
                                        .builder()
                                            .message("Campos Obrigatórios!")
                                            .code(CodeError.REQUIRED_FIELDS)
                                            .requiredFields(requiredFields)
                                        .build();
        return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(error);
    }

    @ExceptionHandler(ErrorHandlingException.class)
    public ResponseEntity<ErrorObject> handlerApplicationInternalError(ErrorHandlingException e) {
        var error = ErrorObject
                .builder()
                    .message(e.getMessage())
                    .code(e.getCode())
                .build();
        return ResponseEntity.status(e.getStatus()).body(error);
    }


    @NotNull
    private static List<RequiredField> getRequiredFields(List<FieldError> fieldErrors) {
        return fieldErrors
                .stream()
                .map(error ->
                        RequiredField
                            .builder()
                                .field(error.getField())
                                .message(error.getDefaultMessage())
                            .build())
                .toList();
    }

}
