package br.com.dornelas.servicecontact.exception.object;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.SuperBuilder;

import java.time.LocalDateTime;

@Data
@SuperBuilder
public class ErrorObject {
    private CodeError code;
    private String message;
    @Builder.Default
    private LocalDateTime timestamp = LocalDateTime.now();
}