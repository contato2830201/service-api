package br.com.dornelas.servicecontact.exception;

import br.com.dornelas.servicecontact.exception.object.CodeError;
import org.springframework.http.HttpStatus;

public class NotFoundException extends ErrorHandlingException {
    public NotFoundException(String message) {
        super(message, CodeError.NOT_FOUND, HttpStatus.NOT_FOUND);
    }
}
