package br.com.dornelas.servicecontact.exception.object;

public enum CodeError {
    REQUIRED_FIELDS,
    NOT_FOUND,
    CONFLITING_DATA,

    AUTHENTICATION,

    EXPIRED_TOKEN,

    INVALID_DATA,

    UNAUTHORIZED;
}
