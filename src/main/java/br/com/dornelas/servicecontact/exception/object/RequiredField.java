package br.com.dornelas.servicecontact.exception.object;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class RequiredField {
    private String field;
    private String message;
}
