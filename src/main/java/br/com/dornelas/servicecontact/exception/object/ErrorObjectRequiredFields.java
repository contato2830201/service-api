package br.com.dornelas.servicecontact.exception.object;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
public class ErrorObjectRequiredFields extends ErrorObject{

    private List<RequiredField> requiredFields;
}
