package br.com.dornelas.servicecontact.repository;

import br.com.dornelas.servicecontact.dto.ContatoProjectionDTO;
import br.com.dornelas.servicecontact.dto.filter.ContatoFilter;
import br.com.dornelas.servicecontact.entity.Contato;
import br.com.dornelas.servicecontact.entity.Contato_;
import br.com.dornelas.servicecontact.entity.filter.ContatoFilterSpec;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.util.ObjectUtils;

import java.util.List;
import java.util.Optional;

public interface ContatoRepository extends JpaRepository<Contato, Long>, JpaSpecificationExecutor<Contato>{
    /*default <T> Page<T> findAllProjectedBy(Specification<Contato> spec, Pageable page, Class<T> classe) {
        return findBy(spec, q -> q.as(classe).page(page));
    }

    default <T> Optional<T> findByIdWithProjection(Long id, Class<T> classe) {
        return findBy(Specs.byId(id), q -> q.as(classe).first());
    }*/

    /*default <T> Page<T> findAllProjectedDTOBy(Specification<Contato> spec, Pageable page, Class<T> classe){
        return findBy(spec, q -> q.(classe).page(page));
    }*/

    //<R> Page<R> findAll(Specification<Contato> spec, Class<R> projectionClass, Pageable pageable);
    //List<ContatoProjectionDTO> findAllProjectedBy(Specification<Contato> specification);

    default Page<Contato> findAll(String name, Boolean favorite, Pageable page){
        ContatoFilter contatoFilter = ContatoFilter
                .builder()
                .name(name)
                .favorite(favorite)
                .build();
        var filter = new ContatoFilterSpec(contatoFilter);
        return this.findAll(filter, page);
    }

    interface Specs {
        static Specification<Contato> byId(Long id) {
            return (root, query, builder) -> builder.and(builder.isNotNull(root.get(Contato_.id)),builder.equal(root.get(Contato_.id), id));
        }

        static Specification<Contato> byIdDiff(Long id) {
            return (root, query, builder) -> builder.and(builder.equal(root.get(Contato_.id), id).not());
        }

        static Specification<Contato> byCellPhone(String cellPhone) {
            return (root, query, builder) -> builder.and(builder.isNotNull(root.get(Contato_.cellPhone)),builder.equal(root.get(Contato_.cellPhone), cellPhone));
        }

        static Specification<Contato> byHomePhone(String homePhone) {
            return (root, query, builder) -> builder.and(builder.isNotNull(root.get(Contato_.homePhone)),builder.equal(root.get(Contato_.homePhone), homePhone));
        }

        static Specification<Contato> byMail(String mail) {
            return (root, query, builder) -> builder.and(builder.isNotNull(root.get(Contato_.mail)),builder.equal(root.get(Contato_.mail), mail));
        }

        static Specification<Contato> byIsActive() {
            return (root, query, builder) -> builder.isTrue(root.get(Contato_.active));
        }
    }
}
