package br.com.dornelas.servicecontact.repository;

import br.com.dornelas.servicecontact.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findUserByEmailOrUsername(String email, String username);
}
