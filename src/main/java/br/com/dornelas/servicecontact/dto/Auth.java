package br.com.dornelas.servicecontact.dto;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class Auth {
    private String token;
    private String fullName;
    @Builder.Default
    private LocalDateTime loggedIn = LocalDateTime.now();
}
