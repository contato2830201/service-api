package br.com.dornelas.servicecontact.dto.filter;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ContatoFilter {
    private String name;
    private Boolean favorite;
}
