package br.com.dornelas.servicecontact.dto;

import com.fasterxml.jackson.annotation.JsonBackReference;
import kotlin.BuilderInference;
import lombok.Builder;

import java.time.LocalDateTime;
import java.util.Objects;

public interface ContatoProjection {
    Long getId();
    String getName();
    String getMail();
    String getCellPhone();
    String getHomePhone();
    Boolean getFavorite();
    Boolean getActive();
    @JsonBackReference
    LocalDateTime getRegistrationTime();

    Long setId(Long id);
    String setName(String name);
    String setMail(String mail);
    String setCellPhone(String cellPhone);
    String setHomePhone(String homePhone);
    @JsonBackReference
    LocalDateTime setRegistrationTime(LocalDateTime registrationTime);

    default boolean compareObject(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ContatoProjectionDTO that = (ContatoProjectionDTO) o;
        return Objects.equals(getId(), that.getId()) && Objects.equals(getName(), that.getName()) && Objects.equals(getMail(), that.getMail()) && Objects.equals(getCellPhone(), that.getCellPhone()) && Objects.equals(getHomePhone(), that.getHomePhone()) && Objects.equals(getFavorite(), that.getFavorite()) && Objects.equals(getActive(), that.getActive()) && Objects.equals(getRegistrationTime(), that.getRegistrationTime());
    }
}
