package br.com.dornelas.servicecontact.dto;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.Column;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import java.time.LocalDateTime;
import java.util.Objects;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class ContatoProjectionDTO {
    private Long id;
    private String name;
    private String mail;
    private String cellPhone;
    private String homePhone;
    @Builder.Default
    private Boolean favorite = false;
    @Builder.Default
    private Boolean active = true;
    @JsonBackReference
    private LocalDateTime registrationTime;
}
