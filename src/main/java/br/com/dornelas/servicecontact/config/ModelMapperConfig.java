package br.com.dornelas.servicecontact.config;

import br.com.dornelas.servicecontact.entity.Contato;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ModelMapperConfig {
    @Bean
    public ModelMapper modelMapper() {
        return mapperDefaultSkip(new ModelMapper());
    }

    private ModelMapper mapperDefaultSkip(ModelMapper modelmapper){
        modelmapper.createTypeMap(Contato.class, Contato.class)
                .addMappings(mapper -> mapper.skip(Contato::setId))
                .addMappings((mapper -> mapper.skip(Contato::setRegistrationTime)));
        return modelmapper;
    }
}
