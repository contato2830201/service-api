package br.com.dornelas.servicecontact.config.security;


import br.com.dornelas.servicecontact.entity.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import jakarta.servlet.http.HttpServletRequest;
import kotlin.text.Charsets;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.crypto.SecretKey;
import javax.naming.AuthenticationException;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class JwtUtil {


    //@Value("${app.security.jwt.private-key}")
    private String SECRET_KEY = "ruI0HKjy9vBNRZwpGLjCYiG8xruI0HKjy9vBNRZwpGLjCYiG8xruI0HKjy9vBNRZwpGLjCYiG8xruI0HKjy9vBNRZwpGLjCYiG8x";

    //@Value("${app.security.jwt.expiration}")
    private Long EXPIRATION_TIME = 1800000L;

    private final String TOKEN_HEADER = "Authorization";
    private final String TOKEN_PREFIX = "Bearer ";

    public String createToken(User user) {
        var randomUUID = UUID.randomUUID().toString();
        return Jwts.builder()
                        .id(randomUUID)
                        .subject(randomUUID)
                        .issuedAt(new Date())
                        .header().add("typ","JWT")
                        .and()
                        .claims()
                            .subject(user.getEmail())
                            .add("given_name",user.getFirstName())
                            .add("family_name",user.getLastName())
                            .add("userName", user.getUsername())
                            .add("email", user.getEmail())
                            .add("name", (user.getFirstName() + " " + Optional.ofNullable(user.getLastName()).orElse("")).trim())
                            .add("email", user.getEmail())
                            .add("typ", "Bearer")
                        .and()
                        .expiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .signWith(getSecretKey())
                .compact();
    }

    private Claims parseJwtClaims(String token) {
        return Jwts
                .parser()
                    .verifyWith(getSecretKey())
                .build()
                    .parseSignedClaims(token)
                    .getPayload();
    }

    public Claims resolveClaims(HttpServletRequest req) {
        try {
            String token = resolveToken(req);
            if (token != null) {
                return parseJwtClaims(token);
            }
            return null;
        } catch (ExpiredJwtException ex) {
            req.setAttribute("expired", ex.getMessage());
            throw ex;
        } catch (Exception ex) {
            req.setAttribute("invalid", ex.getMessage());
            throw ex;
        }
    }

    public String resolveToken(HttpServletRequest request) {

        String bearerToken = request.getHeader(TOKEN_HEADER);
        if (bearerToken != null && bearerToken.startsWith(TOKEN_PREFIX)) {
            return bearerToken.substring(TOKEN_PREFIX.length());
        }
        return null;
    }

    public boolean validateClaims(Claims claims) throws ExpiredJwtException {
        try {
            return claims.getExpiration().after(new Date());
        } catch (Exception e) {
            throw e;
        }
    }

    public String getEmail(Claims claims) {
        return claims.getSubject();
    }

    private List<String> getRoles(Claims claims) {
        return ((List<String>) claims.get("roles"));
    }


    @NotNull
    private SecretKey getSecretKey() {
        System.out.println(SECRET_KEY.getBytes(Charsets.UTF_8).length);
        return Keys.hmacShaKeyFor(SECRET_KEY.getBytes(Charsets.UTF_8));
    }
}