package br.com.dornelas.servicecontact.config.security;


import br.com.dornelas.servicecontact.exception.AuthException;
import br.com.dornelas.servicecontact.exception.ExpiredTokenException;
import br.com.dornelas.servicecontact.exception.object.CodeError;
import br.com.dornelas.servicecontact.exception.object.ErrorObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.servlet.HandlerExceptionResolver;

import java.io.IOException;
import java.nio.file.AccessDeniedException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Component
@AllArgsConstructor
public class JwtAuthorizationFilter extends OncePerRequestFilter {

    private final JwtUtil jwtUtil;
    private final ObjectMapper mapper;
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        try {
            if (validateEmptyToken(request, response, filterChain)) return;
            addUserByContext(request);
        }catch (ExpiredJwtException e){
            throwException("Sua chave de acesso está expirada", CodeError.EXPIRED_TOKEN, HttpStatus.UNAUTHORIZED, response);
        }catch (AccessDeniedException e) {
            throwException("Você não tem permissão para usar esse serviço.", CodeError.UNAUTHORIZED, HttpStatus.FORBIDDEN, response);
        }catch (Exception e){
            throwException(e.getMessage(), CodeError.UNAUTHORIZED, HttpStatus.UNAUTHORIZED, response);
        }
        filterChain.doFilter(request, response);
    }

    private boolean validateEmptyToken(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        String token = jwtUtil.resolveToken(request);
        if (ObjectUtils.isEmpty(token)) {
            filterChain.doFilter(request, response);
            return true;
        }
        return false;
    }

    private void addUserByContext(HttpServletRequest request) {
        Claims claims = jwtUtil.resolveClaims(request);
        if(claims != null & jwtUtil.validateClaims(claims)){
            String email = claims.getSubject();
            Authentication authentication =
                    new UsernamePasswordAuthenticationToken(email,"",new ArrayList<>());
            SecurityContextHolder.getContext().setAuthentication(authentication);
        }
    }

    private void throwException(String message, CodeError unauthorized, HttpStatus status, HttpServletResponse response) throws IOException {
        var error = ErrorObject.builder().message(message).code(unauthorized).build();
        addErroIntoResponse(response, error, status);
    }

    private void addErroIntoResponse(HttpServletResponse response, ErrorObject error, HttpStatus code) throws IOException {
        response.setStatus(code.value());
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        mapper.writeValue(response.getWriter(), error);
    }
}