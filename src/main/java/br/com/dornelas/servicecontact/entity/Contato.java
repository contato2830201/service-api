package br.com.dornelas.servicecontact.entity;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonInclude;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.data.annotation.CreatedDate;

@Data
@Builder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class Contato {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "contato_id")
    private Long id;

    @Column(name = "contato_nome")
    @NotBlank
    private String name;

    @Column(name = "contato_email")
    @Email
    private String mail;

    @Column(name = "contato_celular")
    @NotBlank
    private String cellPhone;

    @Column(name = "contato_telefone")
    private String homePhone;

    @Column(name = "contato_sn_favorito")
    @NotNull
    @Builder.Default
    private Boolean favorite = false;

    @Column(name = "contato_sn_ativo")
    @NotNull
    @Builder.Default
    private Boolean active = true;

    @Column(name = "contato_dh_cad")
    @CreationTimestamp
    @JsonBackReference
    private LocalDateTime registrationTime;

    public void toogleFavorite(){
        this.favorite = !this.favorite;
    }

    public void toogleActive(){
        this.active = !this.active;
    }

    /*@Override
    public boolean equals(Object obj){
        return EqualsBuilder.reflectionEquals(obj, this);
    }*/
    
}
