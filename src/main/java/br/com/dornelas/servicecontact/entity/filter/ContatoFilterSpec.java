package br.com.dornelas.servicecontact.entity.filter;

import static org.springframework.util.ObjectUtils.isEmpty;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.domain.Specification;

import br.com.dornelas.servicecontact.dto.filter.ContatoFilter;
import br.com.dornelas.servicecontact.entity.Contato;
import br.com.dornelas.servicecontact.entity.Contato_;
import jakarta.persistence.Transient;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

@AllArgsConstructor
public class ContatoFilterSpec implements Specification<Contato> {

    @Transient
    private ContatoFilter filter;

    @Override
    public Predicate toPredicate(@NotNull Root<Contato> root, @NotNull CriteriaQuery<?> query, @NotNull CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicates = new ArrayList<>();
        findActiveTrue(root, criteriaBuilder, predicates);
        findName(root, criteriaBuilder, predicates);
        findFavorite(root, criteriaBuilder, predicates);
        return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
    }

    private static void findActiveTrue(@NotNull Root<Contato> root, @NotNull CriteriaBuilder criteriaBuilder, List<Predicate> predicates) {
        predicates.add(criteriaBuilder.isTrue(root.get(Contato_.active)));
    }

    private void findFavorite(Root<Contato> root, CriteriaBuilder criteriaBuilder, List<Predicate> predicates) {
        if(!isEmpty(filter.getFavorite())) {
            predicates.add(
                filter.getFavorite().equals(Boolean.TRUE) ? 
                    criteriaBuilder.isTrue(root.get(Contato_.favorite)) : 
                    criteriaBuilder.isFalse(root.get(Contato_.favorite)));
        }
    }

    private void findName(Root<Contato> root, CriteriaBuilder criteriaBuilder, List<Predicate> predicates) {
        if(!isEmpty(filter.getName())) {
            predicates.add(
                    criteriaBuilder.like(
                            criteriaBuilder.upper(root.get(Contato_.name)),
                                "%" + filter.getName().toUpperCase() + "%"));
        }
    }
    
}
