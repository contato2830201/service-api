package br.com.dornelas.servicecontact.controller;

import br.com.dornelas.servicecontact.config.security.JwtUtil;
import br.com.dornelas.servicecontact.dto.Auth;
import br.com.dornelas.servicecontact.entity.User;
import br.com.dornelas.servicecontact.exception.InvalidDataException;
import br.com.dornelas.servicecontact.service.implement.UserServiceImplement;
import lombok.AllArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Optional;

@Controller
@RequestMapping("/rest/auth")
@AllArgsConstructor
public class AuthController {

    private final AuthenticationManager authenticationManager;

    private final JwtUtil jwtUtil;

    private final UserServiceImplement userServiceImplement;

    @ResponseBody
    @RequestMapping(value = "/login",method = RequestMethod.POST)
    public Auth login(@RequestBody User login)  {
            try {
              Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(login.getEmail(), login.getPassword()));
            }catch (BadCredentialsException e){
              throw new InvalidDataException("Usuário ou senha inválido!");
            }
            User user = userServiceImplement.findByUserNameOrEmail(login.getEmail());
            return Auth.builder()
                            .token(jwtUtil.createToken(user))
                            .fullName((user.getFirstName() + " " + Optional.ofNullable(user.getLastName()).orElse("")))
                        .build();
    }
}