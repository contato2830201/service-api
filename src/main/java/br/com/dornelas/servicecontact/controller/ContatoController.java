package br.com.dornelas.servicecontact.controller;

import br.com.dornelas.servicecontact.dto.ContatoProjection;
import br.com.dornelas.servicecontact.dto.ContatoProjectionDTO;
import br.com.dornelas.servicecontact.dto.filter.ContatoFilter;
import br.com.dornelas.servicecontact.entity.Contato;
import br.com.dornelas.servicecontact.entity.Contato_;
import br.com.dornelas.servicecontact.entity.filter.ContatoFilterSpec;
import br.com.dornelas.servicecontact.service.ContatoService;
import jakarta.validation.Valid;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("contato")
public class ContatoController {

    private final ContatoService contatoService;

    public ContatoController(ContatoService contatoService) {
        this.contatoService = contatoService;
    }

    @GetMapping
    public Page<ContatoProjectionDTO> findAll(
        @RequestParam(required = false) String name,
        @RequestParam(required = false) Boolean favorite,
        @PageableDefault(direction = Direction.DESC, sort = { Contato_.REGISTRATION_TIME }) Pageable page) {
        return contatoService.findAll(name, favorite, page);
    }

    @GetMapping("/{id}")
    public ContatoProjectionDTO findById(@PathVariable Long id) {
        return contatoService.findById(id);
    }


    @GetMapping("/valida-campo")
    public void findById(@RequestParam String valor,
                         @RequestParam(required = false) Long id) {
        contatoService.findByValidaEmailOrCellOrHomePhone(valor, id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Contato create(@Valid @RequestBody Contato novoContato) {
        return contatoService.create(novoContato);
    }

    @PostMapping("/favorite/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public boolean changeFavorite(@PathVariable Long id) {
        return contatoService.changeFavorite(id);
    }

    @PostMapping("/disabled/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public boolean disabled(@PathVariable Long id) {
        return contatoService.disabled(id);
    }

    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Contato update(@Valid @RequestBody Contato contatoEditado) {
        return contatoService.update(contatoEditado);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        contatoService.delete(id);
    }

}
