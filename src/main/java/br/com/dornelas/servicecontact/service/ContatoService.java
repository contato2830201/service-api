package br.com.dornelas.servicecontact.service;

import br.com.dornelas.servicecontact.dto.ContatoProjectionDTO;
import br.com.dornelas.servicecontact.entity.Contato;
import br.com.dornelas.servicecontact.entity.filter.ContatoFilterSpec;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.dornelas.servicecontact.dto.ContatoProjection;
import br.com.dornelas.servicecontact.dto.filter.ContatoFilter;

@Service
public interface ContatoService {
    Page<ContatoProjectionDTO> findAll(String name, Boolean favorite, Pageable page);
    ContatoProjectionDTO findById(Long id);
    Contato create(Contato novoContato);
    boolean changeFavorite(Long id);
    boolean disabled(Long id);
    Contato update(Contato contatoEditado);
    void delete(Long id);
    void findByValidaEmailOrCellOrHomePhone(String valor, Long id);
}
