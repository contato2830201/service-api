package br.com.dornelas.servicecontact.service.implement;

import br.com.dornelas.servicecontact.dto.ContatoProjection;
import br.com.dornelas.servicecontact.dto.ContatoProjectionDTO;
import br.com.dornelas.servicecontact.dto.filter.ContatoFilter;
import br.com.dornelas.servicecontact.entity.Contato;
import br.com.dornelas.servicecontact.entity.filter.ContatoFilterSpec;
import br.com.dornelas.servicecontact.exception.ConflictingDataException;
import br.com.dornelas.servicecontact.exception.InvalidDataException;
import br.com.dornelas.servicecontact.exception.NotFoundException;
import br.com.dornelas.servicecontact.exception.object.CodeError;
import br.com.dornelas.servicecontact.repository.ContatoRepository;
import br.com.dornelas.servicecontact.service.ContatoService;
import jakarta.validation.Valid;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.annotation.Validated;

import java.util.List;
import java.util.Optional;


import static br.com.dornelas.servicecontact.repository.ContatoRepository.Specs.*;


@Service
public class ContatoServiceImplement implements ContatoService {

    private final ContatoRepository contatoRepository;

    private final ModelMapper modelMapper;

    public ContatoServiceImplement(ContatoRepository contatoRepository, ModelMapper modelMapper) {
        this.contatoRepository = contatoRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public Page<ContatoProjectionDTO> findAll(String name, Boolean favorite, Pageable page) {
        var contatos = contatoRepository.findAll(name, favorite, page);
        return contatos.map(contato -> new ModelMapper().map(contato, ContatoProjectionDTO.class));
    }

    @Override
    public ContatoProjectionDTO findById(Long id) {
        validIdNotNull(id);
        var contato = getContato(id);
        return new ModelMapper().map(contato, ContatoProjectionDTO.class);
    }

    @Override
    public void findByValidaEmailOrCellOrHomePhone(String value, Long id) {
        this.existingContactValidation(Contato.builder().mail(value).cellPhone(value).homePhone(value).id(id).build());
    }

    @Override
    @Validated
    public Contato create(@Valid Contato contato) {
        if(!ObjectUtils.isEmpty(contato.getId()))
            throw new InvalidDataException("Identificador encontrado, esse metodo é usado para criar um contato.");
        existingContactValidation(contato);
        return contatoRepository.save(contato);
    }

    @Override
    public boolean changeFavorite(Long id) {
        validIdNotNull(id);
        Contato contato = getContato(id);
        contato.toogleFavorite();
        contatoRepository.save(contato);
        return contato.getFavorite();
    }

    @Override
    public boolean disabled(Long id) {
        validIdNotNull(id);
        Contato contato = getContato(id);
        contato.toogleActive();
        contatoRepository.save(contato);
        return contato.getActive();
    }

    @Override
    public Contato update(Contato contatoEditado) {
        validIdNotNull(contatoEditado.getId());
        Contato contato = getContato(contatoEditado.getId());
        existingContactValidation(contatoEditado);
        modelMapper.map(contatoEditado, contato);
        return contatoRepository.save(contato);
    }

    @Override
    public void delete(Long id) {
        validIdNotNull(id);
        var contato = getContato(id);
            contatoRepository.delete(contato);
    }

    private Contato getContato(Long id) {
        return contatoRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Não foi possível encontrar nenhum contato com esse identificador."));
    }

    private <T> T getContatoByProjection(Long id, Class<T> classe) {
        return null;//contatoRepository.findByIdWithProjection(id, classe)
                //.orElseThrow(() -> new NotFoundException("Não foi possível encontrar nenhum contato com esse identificador."));
    }

    private static void validIdNotNull(Long id) {
        if(ObjectUtils.isEmpty(id))
            throw new InvalidDataException("É necessario informar o identificador desse contato.");
    }

    private void existingContactValidation(Contato contato) {
        var filterDiffIdOrCellPhoneOrHomePhoneOrMail = byCellPhone(contato.getCellPhone()).or(byHomePhone(contato.getHomePhone())).or(byMail(contato.getMail())).and(byIsActive());
        if(!ObjectUtils.isEmpty(contato.getId()))
            filterDiffIdOrCellPhoneOrHomePhoneOrMail = filterDiffIdOrCellPhoneOrHomePhoneOrMail.and(byIdDiff(contato.getId()));
        if(contatoRepository.exists(filterDiffIdOrCellPhoneOrHomePhoneOrMail))
            throw new ConflictingDataException("Já existe um outro contato cadastrado com um desses números ou e-mail");
    }
}
