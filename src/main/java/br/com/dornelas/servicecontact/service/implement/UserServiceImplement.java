package br.com.dornelas.servicecontact.service.implement;

import br.com.dornelas.servicecontact.entity.User;
import br.com.dornelas.servicecontact.exception.NotFoundException;
import br.com.dornelas.servicecontact.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImplement implements UserDetailsService {

    private final UserRepository userRepository;



    public UserServiceImplement(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String mail) throws UsernameNotFoundException {
        User user = userRepository.findUserByEmailOrUsername(mail, mail)
                                        .orElseThrow(()-> new NotFoundException("Usuario ou Senha inválida"));
        List<String> roles = new ArrayList<>();
        roles.add("USER");
        UserDetails userDetails =
                org.springframework.security.core.userdetails.User.builder()
                        .username(user.getEmail())
                        .password(user.getPassword())
                        .roles(roles.toArray(new String[0]))
                        .build();
        return userDetails;
    }

    public User findByUserNameOrEmail(String username){
        return userRepository.findUserByEmailOrUsername(username, username)
                                    .orElseThrow(()-> new NotFoundException("Identificador invalido, usuário não encontrado."));
    }
}
