package br.com.dornelas.servicecontact.repository;

import br.com.dornelas.servicecontact.entity.Contato;
import br.com.dornelas.servicecontact.exception.NotFoundException;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static br.com.dornelas.servicecontact.util.ContatoConstants.*;
import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DataJpaTest
public class ContatoRepositoryTest {

    @Autowired
    private ContatoRepository contatoRepository;

    @Autowired
    private TestEntityManager testEntityManager;


    @AfterEach
    public void afterEach() {
        CONTATO.setId(null);
    }

    @Test
    public void createContato_WithValidData_ReturnContato() {
        Contato contato = contatoRepository.save(CONTATO_WITH_ID);
        Contato objSave = testEntityManager.find(Contato.class, CONTATO_WITH_ID.getId());
        assertThat(objSave).isNotNull();
        assertThat(contato.getName()).isEqualTo(objSave.getName());
        assertThat(contato.getMail()).isEqualTo(objSave.getMail());
        assertThat(contato.getCellPhone()).isEqualTo(objSave.getCellPhone());
        assertThat(contato.getHomePhone()).isEqualTo(objSave.getHomePhone());
        assertThat(contato.getActive()).isEqualTo(objSave.getActive());
        assertThat(contato.getFavorite()).isEqualTo(objSave.getFavorite());
    }


    @Test
    public void createContato_WithInvalidData_ThrowsException() {
        Contato emptyContato = Contato.builder().build();
        Contato invalidContato = Contato.builder().name("").mail("").cellPhone("").build();

        assertThatThrownBy(() -> contatoRepository.save(emptyContato)).isInstanceOf(RuntimeException.class);
        assertThatThrownBy(() -> contatoRepository.save(invalidContato)).isInstanceOf(RuntimeException.class);
    }

    @Test
    public void getContato_ByExistingId_ReturnsContato() {
        Contato contato = testEntityManager.persistFlushFind(CONTATO);
        Optional<Contato> opContato = contatoRepository.findById(contato.getId());

        assertThat(opContato.isPresent()).isTrue();
        assertThat(opContato.get()).isEqualTo(contato);
    }

    @Test
    public void getContato_ByUnexistingId_ReturnsContato() {
        Optional<Contato> opContato = contatoRepository.findById(Long.MAX_VALUE);
        assertThat(opContato.isEmpty()).isTrue();
    }

    @Test
    public void getContato_ByExistingMailOrCellPhoneOrHomePhone_ReturnsContato() {

    }


    @Test
    public void getContato_ByUnexistingMailOrCellPhoneOrHomePhone_ReturnsContato() {

    }
}