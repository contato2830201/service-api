package br.com.dornelas.servicecontact.util;

import br.com.dornelas.servicecontact.dto.ContatoProjection;
import br.com.dornelas.servicecontact.dto.ContatoProjectionDTO;
import br.com.dornelas.servicecontact.dto.filter.ContatoFilter;
import br.com.dornelas.servicecontact.entity.Contato;
import br.com.dornelas.servicecontact.entity.User;
import br.com.dornelas.servicecontact.entity.filter.ContatoFilterSpec;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.projection.ProjectionFactory;
import org.springframework.data.projection.SpelAwareProxyProjectionFactory;

import java.time.LocalDateTime;
import java.util.List;

public class ContatoConstants {
    public static final Contato CONTATO = Contato.builder().name("Teste").mail("teste@teste.com").cellPhone("111111111111").homePhone("22222222222").build();
    public static final Contato CONTATO_WITH_ID = Contato.builder().id(1L).name("Teste").mail("teste@teste.com").cellPhone("111111111111").homePhone("22222222222").active(true).favorite(true).build();
    public static final Contato CONTATO_WITH_ID_EDIT = Contato.builder().id(1L).name("Teste 2").mail("teste@teste.com 2").cellPhone("1232332231").homePhone("453345345345").active(true).favorite(true).build();
    public static final ContatoProjectionDTO CONTATO_PROJECTION_DTO_WITH_ID = ContatoProjectionDTO.builder().id(1L).name("Teste").mail("teste@teste.com").cellPhone("111111111111").homePhone("22222222222").active(true).favorite(true).build();
    public static final ContatoProjectionDTO CONTATO_PROJECTION_DTO_WITH_ID_2 = ContatoProjectionDTO.builder().id(1L).name("Teste2").mail("teste@teste.com2").cellPhone("111111111111").homePhone("22222222222").active(true).favorite(true).build();
    public static final ContatoProjectionDTO CONTATO_PROJECTION_DTO_WITH_ID_3 = ContatoProjectionDTO.builder().id(1L).name("Teste3").mail("teste@teste.com3").cellPhone("111111111111").homePhone("22222222222").active(true).favorite(true).build();
    public static final Contato INVALID_CONTATO_REQUIRED = Contato.builder().name("").mail("qnsjkjsxa").cellPhone("").build();
    public static final ContatoFilterSpec FILTER_CONTATO_SPEC = new ContatoFilterSpec(ContatoFilter.builder().name(CONTATO_PROJECTION_DTO_WITH_ID.getName()).favorite(CONTATO_PROJECTION_DTO_WITH_ID.getFavorite()).build());
    public static final User USER = User.builder().username("teste").firstName("teste").email("teste@teste.com").build();
    public static final Page<Contato> CONTATOS = new PageImpl<>(List.of(CONTATO_WITH_ID, CONTATO_WITH_ID, CONTATO_WITH_ID), PageRequest.of(0,10), 3);
}
