package br.com.dornelas.servicecontact.Controller;

import br.com.dornelas.servicecontact.config.security.JwtUtil;
import br.com.dornelas.servicecontact.controller.ContatoController;
import br.com.dornelas.servicecontact.entity.Contato;
import br.com.dornelas.servicecontact.exception.ConflictingDataException;
import br.com.dornelas.servicecontact.exception.InvalidDataException;
import br.com.dornelas.servicecontact.exception.NotFoundException;
import br.com.dornelas.servicecontact.repository.ContatoRepository;
import br.com.dornelas.servicecontact.service.ContatoService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static br.com.dornelas.servicecontact.util.ContatoConstants.*;
import static org.mockito.Mockito.when;
import static org.hamcrest.Matchers.hasSize;

@WebMvcTest({ContatoController.class})
@AutoConfigureMockMvc(addFilters = false)
public class ContatoControllerTests {
  @Autowired
  private MockMvc mockMvc;

  @Autowired
  private ObjectMapper objectMapper;

  @MockBean
  private ContatoService contatoService;

  @Mock
  private ContatoRepository contatoRepository;

  @MockBean
  private JwtUtil jwtUtil;

  @Test
  public void createContato_WithValidData_ReturnCreated() throws Exception {
    when(contatoService.create(CONTATO)).thenReturn(CONTATO);
    mockMvc.perform(post("/contato")
                    .content(objectMapper.writeValueAsBytes(CONTATO))
                    .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$").value(CONTATO));
  }

  @Test
  public void createContato_WithInvalidData_ReturnBadRequest() throws Exception {
    Contato emptyContato = Contato.builder().build();
    Contato invalidContato = Contato.builder().name("").mail("").cellPhone("").build();
    mockMvc.perform(post("/contato")
                    .content(objectMapper.writeValueAsBytes(emptyContato))
                    .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isUnprocessableEntity());
    mockMvc.perform(post("/contato")
                    .content(objectMapper.writeValueAsBytes(invalidContato))
                    .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isUnprocessableEntity());
  }

  @Test
  public void createContato_WithExistingCellPhone_ReturnsConflitException() throws Exception {
    when(contatoService.create(any())).thenThrow(new ConflictingDataException(""));
    mockMvc.perform(post("/contato")
                    .content(objectMapper.writeValueAsBytes(CONTATO))
                    .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isConflict());
  }

  @Test
  public void getContato_ByExistingId_ReturnsContato() throws Exception {
    when(contatoService.findById(CONTATO_PROJECTION_DTO_WITH_ID.getId())).thenReturn(CONTATO_PROJECTION_DTO_WITH_ID);
    mockMvc.perform(get("/contato/{id}", CONTATO_PROJECTION_DTO_WITH_ID.getId())
                    .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$").value(CONTATO_PROJECTION_DTO_WITH_ID));
  }

  @Test
  public void getContato_ByUnexistingId_ReturnsContato() throws Exception {
    when(contatoService.findById(CONTATO_PROJECTION_DTO_WITH_ID.getId())).thenThrow(new NotFoundException(""));
    mockMvc.perform(get("/contato/{id}", CONTATO_PROJECTION_DTO_WITH_ID.getId())
                    .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isNotFound());
  }

  @Test
  public void getContato_BySendIdNull_ReturnsInvalidDataException() throws Exception {
    when(contatoService.findById(4L)).thenThrow(new InvalidDataException(""));
    mockMvc
      .perform(get("/contato/{id}", 4)
        .contentType(MediaType.APPLICATION_JSON))
      .andExpect(status().isUnprocessableEntity());
  }

  @Test
  public void getContato_ByListAllWithFilter_ReturnsContatos() throws Exception {
    when(contatoRepository.findAll(null, null, CONTATOS.getPageable())).thenReturn(CONTATOS);
    when(contatoRepository.findAll(CONTATO.getName(), CONTATO.getFavorite(), CONTATOS.getPageable())).thenReturn(CONTATOS);

    mockMvc
        .perform(
            get("/contato")
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());

    mockMvc
        .perform(
            get("/contato")
              .param("name", CONTATO_PROJECTION_DTO_WITH_ID.getName())
              .param("favorite", CONTATO_PROJECTION_DTO_WITH_ID.getFavorite().toString())
              .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
  }

  @Test
  public void changeContatoFavorite_WhenNotSendIdFavorite_ReturnInvalidDataException() throws Exception {
    when(contatoService.changeFavorite(Long.MAX_VALUE)).thenThrow(new InvalidDataException(""));
    mockMvc
        .perform(
            post("/contato/favorite/{id}", Long.MAX_VALUE)
              .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isUnprocessableEntity());
  }

  @Test
  public void changeContatoActive_WhenNotSendIdActive_ReturnInvalidDataException() throws Exception {
    when(contatoService.disabled(Long.MAX_VALUE)).thenThrow(new InvalidDataException(""));
    mockMvc
        .perform(
            post("/contato/disabled/{id}", Long.MAX_VALUE)
              .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isUnprocessableEntity());
  }

  @Test
  public void changeContatoFavorite_WhenUpdateToogle_ReturnToogle() throws Exception {
    mockMvc
      .perform(
        post("/contato/favorite/{id}", 1L)
          .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isCreated());
  }

  @Test
  public void changeContatoDisabled_WhenUpdateToogle_ReturnToogle() throws Exception {
    mockMvc
      .perform(
          post("/contato/disabled/{id}", 1L)
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isCreated());
  }

  

}
