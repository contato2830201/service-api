package br.com.dornelas.servicecontact.service;

import br.com.dornelas.servicecontact.dto.ContatoProjectionDTO;
import br.com.dornelas.servicecontact.dto.filter.ContatoFilter;
import br.com.dornelas.servicecontact.entity.Contato;
import br.com.dornelas.servicecontact.entity.filter.ContatoFilterSpec;
import br.com.dornelas.servicecontact.exception.ConflictingDataException;
import br.com.dornelas.servicecontact.exception.InvalidDataException;
import br.com.dornelas.servicecontact.exception.NotFoundException;
import br.com.dornelas.servicecontact.repository.ContatoRepository;
import br.com.dornelas.servicecontact.service.implement.ContatoServiceImplement;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static br.com.dornelas.servicecontact.util.ContatoConstants.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ContatoServiceTest {
    @InjectMocks
    private ContatoServiceImplement contatoServiceImplement;

    @Mock
    private ContatoRepository contatoRepository;

    @Mock
    @Qualifier("myEntityMapper")
    private ModelMapper modelMapper;

    @Bean
    public MethodValidationPostProcessor bean() {
        return new MethodValidationPostProcessor();
    }

    @Test
    public void createContato_WithDataValid_ReturnsContatoSaved() {
        when(contatoServiceImplement.create(CONTATO)).thenReturn(CONTATO);
        Contato contato = contatoServiceImplement.create(CONTATO);
        assertThat(contato).isEqualTo(CONTATO);
    }

    @Test
    public void createContato_WithIdObject_ReturnsInvalidDataException() {
        assertThatThrownBy(() -> contatoServiceImplement.create(CONTATO_WITH_ID)).isInstanceOf(InvalidDataException.class);
    }


    @Test
    public void createOrEditContacto_WhenValidated_ThenMustContainThreeErrors() {
        Set<ConstraintViolation<Contato>> constraintViolations = getConstraintViolations(INVALID_CONTATO_REQUIRED);
        assertThat(constraintViolations.size()).isEqualTo(3L);
    }

    @Test
    public void createContacto_WhenValidatedEmailOrCellPhoneOrHomePhoneOfTheContatoActive_ReturnConflitDataException() {
        when(contatoServiceImplement.create(CONTATO)).thenThrow(ConflictingDataException.class);
        assertThatThrownBy(() -> contatoServiceImplement.create(CONTATO)).isInstanceOf(ConflictingDataException.class);
    }

    @Test
    public void getContato_ByExistingId_ReturnsContato(){
        when(contatoRepository.findById(CONTATO_WITH_ID.getId())).thenReturn(Optional.of(CONTATO_WITH_ID));
        ContatoProjectionDTO contato = contatoServiceImplement.findById(CONTATO_WITH_ID.getId());
        assertThat(contato).isNotNull();
        assertThat(contato).isEqualTo(CONTATO_PROJECTION_DTO_WITH_ID);
    }

    @Test
    public void getContato_ByUnexistingId_ReturnNotFoundException() {
        assertThatThrownBy(() -> contatoServiceImplement.findById(2L)).isInstanceOf(NotFoundException.class);
    }

    @Test
    public void getContato_WhenNotSendId_ReturnInvalidDataException() {
        assertThatThrownBy(() -> contatoServiceImplement.findById(null)).isInstanceOf(InvalidDataException.class);
    }


    @Test
    public void changeContato_WhenUpdateToogleFavorite_ReturnToogle() {
        when(contatoRepository.findById(1L)).thenReturn(Optional.of(CONTATO_WITH_ID));
        Boolean favorite = contatoServiceImplement.changeFavorite(1L);
        assertThat(favorite).isFalse();
        favorite = contatoServiceImplement.changeFavorite(1L);
        assertThat(favorite).isTrue();
    }

    @Test
    public void changeContatoFavorite_WhenNotSendIdFavorite_ReturnInvalidDataException() {
        assertThatThrownBy(() -> contatoServiceImplement.changeFavorite(null)).isInstanceOf(InvalidDataException.class);
    }

    @Test
    public void changeContatoFavorite_WhenNotExistingContato_ReturnNotFoundException() {
        assertThatThrownBy(() -> contatoServiceImplement.changeFavorite(2L)).isInstanceOf(NotFoundException.class);
    }

    @Test
    public void changeContatoFavorite_WhenUpdateToogle_ReturnToogle() {
        when(contatoRepository.findById(1L)).thenReturn(Optional.of(CONTATO_WITH_ID));
        Boolean favorite = contatoServiceImplement.disabled(1L);
        assertThat(favorite).isFalse();
        favorite = contatoServiceImplement.disabled(1L);
        assertThat(favorite).isTrue();
    }

    @Test
    public void changeContatoDisable_WhenNotSendId_ReturnInvalidDataException() {
        assertThatThrownBy(() -> contatoServiceImplement.disabled(null)).isInstanceOf(InvalidDataException.class);
    }

    @Test
    public void changeContatoDisable_WhenNotExistingContato_ReturnNotFoundException() {
        assertThatThrownBy(() -> contatoServiceImplement.disabled(2L)).isInstanceOf(NotFoundException.class);
    }

    @Test
    public void editContato_WithDataValid_ReturnsContatoSaved() {
        when(contatoRepository.findById(1L)).thenReturn(Optional.of(CONTATO_WITH_ID));
        Contato contato = contatoServiceImplement.update(CONTATO_WITH_ID_EDIT);
        assertThat(contato).isNotEqualTo(CONTATO_WITH_ID);
    }

    @Test
    public void editContato_WithNotIdObject_ReturnsInvalidDataException() {
        assertThatThrownBy(() -> contatoServiceImplement.update(CONTATO)).isInstanceOf(InvalidDataException.class);
    }

    @Test
    public void createContacto_WhenValidatedEmailOrCellPhoneOrHomePhoneOfTheContatoActiveAndNotId_ReturnConflitDataException() {
        when(contatoRepository.findById(1L)).thenThrow(ConflictingDataException.class);
        assertThatThrownBy(() -> contatoServiceImplement.update(CONTATO_WITH_ID_EDIT)).isInstanceOf(ConflictingDataException.class);
    }

    @Test
    public void deleteContato_WhenNotSendId_ReturnInvalidDataException() {
        assertThatThrownBy(() -> contatoServiceImplement.delete(null)).isInstanceOf(InvalidDataException.class);
    }

    @Test
    public void deleteContato_WhenNotExistingContato_ReturnNotFoundException() {
        assertThatThrownBy(() -> contatoServiceImplement.delete(2L)).isInstanceOf(NotFoundException.class);
    }

    @Test
    public void deleteContato_whenDelete_ReturnsVoid(){
        when(contatoRepository.findById(1L)).thenReturn(Optional.of(CONTATO));
        contatoServiceImplement.delete(1L);
        /*assertThat(contato).isNotNull();
        assertThat(contato).isEqualTo(CONTATO_PROJECTION_DTO_WITH_ID);*/
    }

    @Test
    public void getContato_whenAll_ReturnsAllContatos(){
        Page<Contato> list = new PageImpl<>(List.of(CONTATO_WITH_ID), PageRequest.of(0,10), 0);
        when(contatoRepository.findAll(CONTATO_PROJECTION_DTO_WITH_ID.getName(), CONTATO_PROJECTION_DTO_WITH_ID.getFavorite(), list.getPageable())).thenReturn(list);

        Page<ContatoProjectionDTO> contatos = contatoServiceImplement.findAll(CONTATO_PROJECTION_DTO_WITH_ID.getName(), CONTATO_PROJECTION_DTO_WITH_ID.getFavorite(), list.getPageable());
        assertThat(contatos.isEmpty()).isFalse();
        assertThat(contatos.getTotalElements()).isEqualTo(1L);
        assertThat(contatos.get().findFirst().get()).isEqualTo(CONTATO_PROJECTION_DTO_WITH_ID);
    }

    @Test
    public void getContato_whenNotAll_ReturnsEmpty(){
        Page<Contato> list = new PageImpl<>(List.of(), PageRequest.of(0,10), 0);
        when(contatoRepository.findAll(CONTATO_PROJECTION_DTO_WITH_ID.getName(), CONTATO_PROJECTION_DTO_WITH_ID.getFavorite(), list.getPageable())).thenReturn(list);
        Page<ContatoProjectionDTO> contatos = contatoServiceImplement.findAll(CONTATO_PROJECTION_DTO_WITH_ID.getName(), CONTATO_PROJECTION_DTO_WITH_ID.getFavorite(), list.getPageable());
        assertThat(contatos.isEmpty()).isTrue();
    }

    private static <T> Set<ConstraintViolation<T>> getConstraintViolations(T obj) {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        final Validator validator = factory.getValidator();
        factory.close();
        return validator.validate(obj);
    }

}
