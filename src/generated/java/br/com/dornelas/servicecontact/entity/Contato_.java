package br.com.dornelas.servicecontact.entity;

import jakarta.annotation.Generated;
import jakarta.persistence.metamodel.EntityType;
import jakarta.persistence.metamodel.SingularAttribute;
import jakarta.persistence.metamodel.StaticMetamodel;
import java.time.LocalDateTime;

@StaticMetamodel(Contato.class)
@Generated("org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
public abstract class Contato_ {

	
	/**
	 * @see br.com.dornelas.servicecontact.entity.Contato#registrationTime
	 **/
	public static volatile SingularAttribute<Contato, LocalDateTime> registrationTime;
	
	/**
	 * @see br.com.dornelas.servicecontact.entity.Contato#mail
	 **/
	public static volatile SingularAttribute<Contato, String> mail;
	
	/**
	 * @see br.com.dornelas.servicecontact.entity.Contato#homePhone
	 **/
	public static volatile SingularAttribute<Contato, String> homePhone;
	
	/**
	 * @see br.com.dornelas.servicecontact.entity.Contato#name
	 **/
	public static volatile SingularAttribute<Contato, String> name;
	
	/**
	 * @see br.com.dornelas.servicecontact.entity.Contato#active
	 **/
	public static volatile SingularAttribute<Contato, Boolean> active;
	
	/**
	 * @see br.com.dornelas.servicecontact.entity.Contato#id
	 **/
	public static volatile SingularAttribute<Contato, Long> id;
	
	/**
	 * @see br.com.dornelas.servicecontact.entity.Contato
	 **/
	public static volatile EntityType<Contato> class_;
	
	/**
	 * @see br.com.dornelas.servicecontact.entity.Contato#favorite
	 **/
	public static volatile SingularAttribute<Contato, Boolean> favorite;
	
	/**
	 * @see br.com.dornelas.servicecontact.entity.Contato#cellPhone
	 **/
	public static volatile SingularAttribute<Contato, String> cellPhone;

	public static final String REGISTRATION_TIME = "registrationTime";
	public static final String MAIL = "mail";
	public static final String HOME_PHONE = "homePhone";
	public static final String NAME = "name";
	public static final String ACTIVE = "active";
	public static final String ID = "id";
	public static final String FAVORITE = "favorite";
	public static final String CELL_PHONE = "cellPhone";

}

